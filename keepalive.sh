#! /bin/sh
#
# Script to check the wifi connection and reconnect if necessary
# (It seems that a dead connection can still 'look' up to iwinfo)
#test_server='192.168.0.1'
reboot='false'

test_server=$(route -n | awk 'FNR == 3 {print $2}')
if [ "$test_server" = "" ]; then
    test_server="www.google.com" # force failure if no route set
fi


if ping -q -c 1 -W 2  "$test_server" > /dev/null; then
    echo 0 > /tmp/keepalive
else
    echo "IPv4 is down"
    wifi down
    sleep 2
    wifi up
    echo $(( $(cat /tmp/keepalive) +1)) > /tmp/keepalive || echo 1 > /tmp/keepalive
    echo "toggle wifi $(date)" >> /tmp/wifi.log
    logger "toggle wifi $(date)"
fi

if [ $reboot = 'false' ]; then
    exit 1
fi


if [ $(cat /tmp/keepalive) == 3 ]; then # no wifi for 20 minutes
    echo "reboot $(date)" >> /tmp/wifi.log
    logger "reboot $(date)"
    reboot
fi
